package ProducerConsumer;

public class Driver {
    public static void main(String[] args) throws InterruptedException {
        SharedSpace space = new SharedSpace();

        ProducerConsumer producer = new ProducerConsumer(0, space);
        ProducerConsumer consumer = new ProducerConsumer(1, space);

        producer.start();
        consumer.start();


        producer.join();
        consumer.join();

        System.out.println("--END--");
    }
}
