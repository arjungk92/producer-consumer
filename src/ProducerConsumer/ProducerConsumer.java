package ProducerConsumer;



public class ProducerConsumer extends Thread {
    private int id;
    private SharedSpace space;

    public ProducerConsumer(int id, SharedSpace space) {
        this.id = id;
        this.space = space;
    }

    public void produce() {

        int i = 0;

        while(i < 100) {
            space.add(i);
            System.out.println("Produced: " + i);
            i++;
        }
    }

    public void consume() {
        int i = 0;

        while(i < 100) {
            int val = space.remove();
            System.out.println("Consumed: " + val);
            i++;

        }
    }

    public void run() {
        if(this.id == 0) {
            this.produce();
        }
        else {
            this.consume();
        }
    }

}
