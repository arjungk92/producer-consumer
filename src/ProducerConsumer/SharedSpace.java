package ProducerConsumer;

import java.util.concurrent.Semaphore;

public class SharedSpace {
    private int[] a = new int[10];
    private int producerIndex;
    private int consumerIndex;
    private int size = 10;
    private Semaphore semEmpty;
    private Semaphore semFilled;
    private static int count = 0;

    public SharedSpace() {
        this.consumerIndex = 0;
        this.producerIndex = 0;
        this.semEmpty = new Semaphore(this.size);
        this.semFilled = new Semaphore(0);
    }

    public void add(int val) {
        try {
            this.semEmpty.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized (this) {
            this.a[producerIndex] = val;

            this.semFilled.release();

            this.producerIndex = (this.producerIndex + 1) % this.size;
        }
    }

    public int remove() {

        try {
            this.semFilled.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized (this) {
            int returnVal = this.a[consumerIndex];

            this.consumerIndex = (this.consumerIndex + 1) % this.size;

            this.semEmpty.release();

            return returnVal;
        }
    }
}
